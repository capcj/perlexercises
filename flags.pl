#!/usr/bin/perl -w
use strict;
use warnings;

/*
    Flags - A brief experiment with hashes and closures to improve my perl scripts
*/

my %functions = (
    "-test" => sub {
        printf("%d - %s\n", 1, $_[0]);
    },
    "-test2" => sub {
        printf("%d - %s\n", 2, $_[0]);
    }
);

my $counter = 0;
foreach my $arg (@ARGV) {
    if ($arg =~ /-/) {
        $functions{$arg}->($ARGV[$counter + 1]);
    }
    $counter++;
}


