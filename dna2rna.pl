#!/usr/bin/perl
use strict;
use warnings;

foreach (@ARGV) {
	if (!$_ eq '') {
		print "USER INPUT: $_";
		print "\n==========================================\n";
		print ($_ =~ tr/T/U/r);
	}
}
