#!/usr/bin/perl
use strict;
use warnings;
use Switch;

foreach (@ARGV) {
  switch ($_) {
    case '' {
      warn "You must send a valid argument";
    } 
    case '-h' {
      print "Insert the DNA string to count the amount of nucleotides (A,C,G,T)";
    }
    else {
      print "USER INPUT: $_";
      print "\n==========================================\n";
      my %dnaNs = (
        'A', 0,
        'C', 0,
        'G', 0,
        'T', 0
      );
      foreach my $char (split //, $_) {
        $dnaNs{$char}++;	
      }
      foreach my $key (sort (keys(%dnaNs))) {
        print "\t\t$key \t\t$dnaNs{$key}\n";
      }
    }
  }
}
